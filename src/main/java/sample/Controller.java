package sample;

import com.speedment.runtime.core.ApplicationBuilder;
import com.speedment.runtime.core.exception.SpeedmentException;
import com.speedment.runtime.core.manager.Manager;
import com.speedment.runtime.core.manager.ManagerConfigurator;
import database.MyresumeApplication;
import database.MyresumeApplicationBuilder;
import database.myresume.myresume.member.Member;
import database.myresume.myresume.member.MemberImpl;
import database.myresume.myresume.member.MemberManager;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.web.HTMLEditor;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import java.sql.Date;
import java.util.Optional;

public class Controller {
    public HTMLEditor txtAboutMe;
    public Button btnOK;
    public WebView web;
    public Button doGetAboutMe;
    public TextField txtMobile;
    public TextField txtBirthplace;
    public TextField txtName;
    public TextField txtBirthday;

    public void onGetReadMe(ActionEvent actionEvent) {
        System.out.println(txtAboutMe.getHtmlText());
        if(txtMobile.getText().length()==10){
            MyresumeApplication myresumeApplication = new MyresumeApplicationBuilder().withUsername("root")
                    .withPassword("abcd1234").withLogging(ApplicationBuilder.LogType.PERSIST)
                    .withLogging(ApplicationBuilder.LogType.CONNECTION)
                    .withLogging(ApplicationBuilder.LogType.STREAM).build();

            MemberManager memberManager = myresumeApplication.getOrThrow(MemberManager.class);
            memberManager.persist(new MemberImpl().setAboutme(txtAboutMe.getHtmlText()).setBirthday(Date.valueOf(txtBirthday.getText()))
                    .setBirthplace(txtBirthplace.getText()).setMobile(txtMobile.getText()).setName(txtName.getText()));



            WebEngine webEngine = web.getEngine();
            webEngine.loadContent(txtAboutMe.getHtmlText(),"text/html");
        }

    }

    public void onGetAboutMe(ActionEvent actionEvent) {
        if(txtMobile.getText().length()==10){
            MyresumeApplication myresumeApplication = new MyresumeApplicationBuilder().withUsername("root")
                    .withPassword("abcd1234").withLogging(ApplicationBuilder.LogType.PERSIST)
                    .withLogging(ApplicationBuilder.LogType.CONNECTION)
                    .withLogging(ApplicationBuilder.LogType.STREAM).build();

            MemberManager memberManager = myresumeApplication.getOrThrow(MemberManager.class);

            Optional<Member> memberOptional = memberManager.stream().findFirst();

            WebEngine webEngine = web.getEngine();

            if(memberOptional.isPresent()){
                webEngine.loadContent(memberOptional.get().getAboutme(),"text/html");
            }else{
                webEngine.load("http://www.google.com");
            }
        }else{
            System.out.println("Exceed 10");
        }

    }
}
