package database.myresume.myresume.member;

import database.myresume.myresume.member.generated.GeneratedMemberSqlAdapter;

/**
 * The SqlAdapter for every {@link database.myresume.myresume.member.Member}
 * entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public class MemberSqlAdapter extends GeneratedMemberSqlAdapter {}