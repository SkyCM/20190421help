package database.myresume.myresume.member;

import database.myresume.myresume.member.generated.GeneratedMemberImpl;

/**
 * The default implementation of the {@link
 * database.myresume.myresume.member.Member}-interface.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public final class MemberImpl 
extends GeneratedMemberImpl 
implements Member {}