package database;

import database.generated.GeneratedMyresumeApplication;

/**
 * An {@link com.speedment.runtime.core.ApplicationBuilder} interface for the
 * {@link com.speedment.runtime.config.Project} named myresume.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public interface MyresumeApplication extends GeneratedMyresumeApplication {}